//load data
function load_data(){
    var xhr = new XMLHttpRequest();
    xhr.open('GET','data.php');
    xhr.onload = function(){
        if(xhr.status = 200){
            // console.log(xhr.responseText);
            siswa.innerHTML = xhr.responseText;
        } else {
            console.log("ajax failed");
        }
    }
    xhr.send();

}

let siswa = document.querySelector("#data-siswa");
let form = document.querySelector("form");

//tambah data
let tambah = form.querySelector('button[name=tambah]');

tambah.addEventListener('click',(e)=>{

    e.preventDefault();

    let formData = new FormData();

    formData.append('nama', form.elements[0].value);
    formData.append('kelas', form.elements[1].value);
    formData.append('jurusan', form.elements[2].value);

    for (var value of formData.values()) {
        console.log(value); 
     }
    var xhr = new XMLHttpRequest();
    xhr.open('POST','tambah.php');
    xhr.onload = function(){
        if(xhr.status = 200){
            // console.log(xhr.responseText);
            swal("Sukses!", "Data berhasil ditambah!", "success");
            form.reset();
            load_data();
            
        } else {
            console.log("ajax failed");
        }
    }
    xhr.send(formData);


});

//edit

let btnEdit = document.querySelector("button[name=edit]");

function edit(id){
    // console.log(id);

    let data = new FormData();
    data.append('id',id);

    let xhr = new XMLHttpRequest();
    xhr.open('POST','edit.php');
    xhr.onload = function(){
        if(xhr.status = 200){

            console.log(xhr.responseText);
            let siswa = JSON.parse(xhr.responseText); 
            console.log(siswa);
            document.forms[0].elements[0].value = siswa.id;
            document.forms[0].elements[1].value = siswa.nama;
            document.forms[0].elements[2].value = siswa.kelas;
            document.forms[0].elements[3].value = siswa.jurusan;

            tambah.style.display="none";
            btnEdit.style.display="inline";
        } else {
            console.log("ajax failed");
        }
    }
    xhr.send(data);

}

btnEdit.addEventListener('click',function(e){
    e.preventDefault();

    let data = new FormData();
    data.append('edit',true);
    data.append('id',document.forms[0].elements[0].value);
    data.append('nama',document.forms[0].elements[1].value);
    data.append('kelas',document.forms[0].elements[2].value);
    data.append('jurusan',document.forms[0].elements[3].value);
    let xhr = new XMLHttpRequest();
    xhr.open('POST','edit.php');
    xhr.onload = function(){
        if(xhr.status == 200){
            console.log(xhr.responseText);
            swal("Sukses!", "Data berhasil diedit!", "success");
            load_data();
            btnEdit.style.display="none";
            tambah.style.display="inline";
            form.reset();
        }
    }
    xhr.send(data);

});



//hapus

function hapus(id){
    // console.log(id);

    swal({
        title: "Apakah Kamu yakin?",
        text: "Data akan hilang setelah dihapus!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
      })
      .then((willDelete) => {
        if (willDelete) {

            let data = new FormData();
            data.append('id',id);

            let xhr = new XMLHttpRequest();
            xhr.open('POST','hapus.php');
            xhr.onload = function(){
                if(xhr.status = 200){
                    // console.log(xhr.responseText);
                    load_data();
                } else {
                    console.log("ajax failed");
                }
            }
            xhr.send(data);
          
        } else {
          swal("Data batal dihapus!");
        }
      });

}



