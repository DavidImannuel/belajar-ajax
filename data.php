<?php
require_once("functions.php");

$siswa = fetch_data("siswa");
// var_dump($siswa);
$i = 1;
?>
<table class="table" id="table-siswa">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Nama</th>
      <th scope="col">Kelas</th>
      <th scope="col">Jurusan</th>
      <th scope="col" colspan='2'>Opsi</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach($siswa as $rows):?>
    <tr>
      <th scope="row"><?=$i?></th>
      <td><?=$rows['nama']?></td>
      <td><?=$rows['kelas']?></td>
      <td><?=$rows['jurusan']?></td>
      <td><button class="btn btn-primary" id-siswa="<?=$rows['id']?>" id="edit" onclick="edit(<?=$rows['id']?>)">edit</button></td>
      <td><button class="btn btn-danger" id-siswa="<?=$rows['id']?>" id="hapus" onclick="hapus(<?=$rows['id']?>)">hapus</button></td>
    </tr>
    <?php $i++;?>
    <?php endforeach;?>
  </tbody>
</table>