<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Belajar Ajax</title>
    <link rel="stylesheet" href="assets/css/bootstrap.css">
</head>
<body>

    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <form action="" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="id" >
                    <div class="form-group">
                        <label for="nama">Nama</label>
                        <input type="text" class="form-control" name="nama" id="nama" autocomplete="Off">
                    </div>
                    <div class="form-group">
                        <label for="kelas">Kelas</label>
                        <select type="text" class="form-control" name="kelas" id="kelas">
                            <option value="">Pilih kelas</option>
                            <option value="X">X</option>
                            <option value="XI">XI</option>
                            <option value="XII">XII</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="kelas">Jurusan</label>
                        <select type="text" class="form-control" name="jurusan" id="kelas">
                            <option value="">Pilih jurusan</option>
                            <option value="RPL">RPL</option>
                            <option value="SIJA">SIJA</option>
                            <option value="TKJ">TKJ</option>
                        </select>
                    </div>

                    <button name="tambah" class="btn btn-success">tambah</button>
                    <button name="edit" style="display:none;" class="btn btn-success">edit</button>
                </form>
            </div>
            <div class="col-md-6">
                <div id="data-siswa"></div>
            </div>
        </div>
    </div>

    
    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/bootstrap.js"></script>
    <script src="assets/js/sweetalert.js"></script>
    <script src="script.js"></script>
    <script>
        load_data(); 
    </script>
</body>
</html>